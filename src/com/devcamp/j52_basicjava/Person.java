package com.devcamp.j52_basicjava;

public class Person extends Object {
    public String name; // Tên
    public int age; // Tuổi
    public double weight; // Cân nặng
    public long salary; // thu nhập
    public String[] pets; // thú cưng

    /**
     * Hàm khởi tạo ko có tham số
     */
    public Person() {
        this.name = "QuanNM";
        this.age = 27;
        this.weight = 80;
        this.salary = 10000000;
        this.pets = new String[] {"Dog", "Cat", "Bird"};
    }

    /**
     * Hàm khởi tạo có 3 tham số
     */
    public Person(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    /**
     * Hàm khởi tạo có 4 tham số
     */
    public Person(String name, int age, double weight, long salary) {
        // this.name = name;
        // this.age = age;
        // this.weight = weight;
        this(name, age, weight);
        this.salary = salary;
    }

    /**
     * Hàm khởi tạo có 4 tham số
     */
    public Person(String name, int age, double weight, String[] pets) {
        this(name, age, weight);
        this.pets = pets;
    }

    /**
     * Hàm khởi tạo có 5 tham số
     */
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this(name, age, weight, salary);
        this.pets = pets;
    }

    /**
     * Phương thức hiển thị dữ liệu
     */
    public void showInfo() {
        System.out.println("Name: " + this.name);
        System.out.println("Age: " + this.age);
        System.out.println("Weight: " + this.weight);
        System.out.println("Salary: " + this.salary);
        System.out.println("Pets: " + this.pets);
    }

    /*
     * Override: phương thức giống hết lớp cha (giống cả tên và tham số)
     */
    //@Override
    public String toString() {
        String strPerson = "{";
        strPerson += "Name: " + this.name + ", ";
        strPerson += "Age: " + this.age + ", ";
        strPerson += "Weight: " + this.weight + ", ";
        strPerson += "Salary: " + this.salary + ", ";
        strPerson += "Pets: [";
        if (this.pets != null) {
            for (String pet: pets) {
                strPerson += pet + ", ";
            }
        }
        strPerson += "]}";
        return strPerson;
    }
}
