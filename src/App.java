import java.util.ArrayList;

import com.devcamp.j52_basicjava.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        ArrayList<Person> listPersons = new ArrayList<Person>();
        Person person1 = new Person();
        //System.out.println(person1);
        
        // person1.name = "QuanNM";
        // person1.age = 27;
        //System.out.println(person1);
        //person1.showInfo();
        // System.out.println("Name: " + person1.name);
        // System.out.println("Age: " + person1.age);
        // System.out.println("Weight: " + person1.weight);
        // System.out.println("Salary: " + person1.salary);
        // System.out.println("Pets: " + person1.pets);

        Person person2 = new Person("BoiHB", 23, 56);
        //person2.showInfo();
        // System.out.println("Name: " + person2.name);
        // System.out.println("Age: " + person2.age);
        // System.out.println("Weight: " + person2.weight);
        // System.out.println("Salary: " + person2.salary);
        // System.out.println("Pets: " + person2.pets);

        Person person3 = new Person("ThuNHM", 17, 45, 5000000);
        //person3.showInfo();
        // System.out.println("Name: " + person3.name);
        // System.out.println("Age: " + person3.age);
        // System.out.println("Weight: " + person3.weight);
        // System.out.println("Salary: " + person3.salary);
        // System.out.println("Pets: " + person3.pets);

        Person person4 = new Person("DucNM", 50, 70, new String[] {"Cat", "Dog"});
        //person4.showInfo();
        // System.out.println("Name: " + person4.name);
        // System.out.println("Age: " + person4.age);
        // System.out.println("Weight: " + person4.weight);
        // System.out.println("Salary: " + person4.salary);
        // System.out.println("Pets: " + person4.pets);

        Person person5 = new Person("ThanhHT", 50, 60, 18000000, new String[] {"Cat", "Dog"});
        //person5.showInfo();

        // Add các person vào arraylist
        listPersons.add(person1);
        listPersons.add(person2);
        listPersons.add(person3);
        listPersons.add(person4);
        listPersons.add(person5);
        for (Person person: listPersons) {
            //person.showInfo();
            System.out.println(person);
        }
    }
}
